report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/HighEdWebLink_Link_Article_0_document_0_phone.png",
        "test": "../bitmaps_test/20230218-091400/HighEdWebLink_Link_Article_0_document_0_phone.png",
        "selector": "document",
        "fileName": "HighEdWebLink_Link_Article_0_document_0_phone.png",
        "label": "Link Article",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewlink.pantheonsite.io/2021/03/boring-documents-creative-freedom/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "rawMisMatchPercentage": 2.372727272727273,
          "misMatchPercentage": "2.37",
          "analysisTime": 219
        },
        "diffImage": "../bitmaps_test/20230218-091400/failed_diff_HighEdWebLink_Link_Article_0_document_0_phone.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/HighEdWebLink_Link_Article_0_document_1_tablet.png",
        "test": "../bitmaps_test/20230218-091400/HighEdWebLink_Link_Article_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "HighEdWebLink_Link_Article_0_document_1_tablet.png",
        "label": "Link Article",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewlink.pantheonsite.io/2021/03/boring-documents-creative-freedom/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "rawMisMatchPercentage": 5.865451882944413,
          "misMatchPercentage": "5.87",
          "analysisTime": 488
        },
        "diffImage": "../bitmaps_test/20230218-091400/failed_diff_HighEdWebLink_Link_Article_0_document_1_tablet.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/HighEdWebLink_Link_Article_0_document_2_desktop.png",
        "test": "../bitmaps_test/20230218-091400/HighEdWebLink_Link_Article_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "HighEdWebLink_Link_Article_0_document_2_desktop.png",
        "label": "Link Article",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewlink.pantheonsite.io/2021/03/boring-documents-creative-freedom/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "rawMisMatchPercentage": 0.05750458813203181,
          "misMatchPercentage": "0.06",
          "analysisTime": 833
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/HighEdWebLink_Link_Contribute_Page_0_document_0_phone.png",
        "test": "../bitmaps_test/20230218-091400/HighEdWebLink_Link_Contribute_Page_0_document_0_phone.png",
        "selector": "document",
        "fileName": "HighEdWebLink_Link_Contribute_Page_0_document_0_phone.png",
        "label": "Link Contribute Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewlink.pantheonsite.io/contribute/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/HighEdWebLink_Link_Contribute_Page_0_document_1_tablet.png",
        "test": "../bitmaps_test/20230218-091400/HighEdWebLink_Link_Contribute_Page_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "HighEdWebLink_Link_Contribute_Page_0_document_1_tablet.png",
        "label": "Link Contribute Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewlink.pantheonsite.io/contribute/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "rawMisMatchPercentage": 0.16910918101722067,
          "misMatchPercentage": "0.17",
          "analysisTime": 481
        },
        "diffImage": "../bitmaps_test/20230218-091400/failed_diff_HighEdWebLink_Link_Contribute_Page_0_document_1_tablet.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/HighEdWebLink_Link_Contribute_Page_0_document_2_desktop.png",
        "test": "../bitmaps_test/20230218-091400/HighEdWebLink_Link_Contribute_Page_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "HighEdWebLink_Link_Contribute_Page_0_document_2_desktop.png",
        "label": "Link Contribute Page",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hewlink.pantheonsite.io/contribute/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "rawMisMatchPercentage": 0.47218045112781953,
          "misMatchPercentage": "0.47",
          "analysisTime": 397
        },
        "diffImage": "../bitmaps_test/20230218-091400/failed_diff_HighEdWebLink_Link_Contribute_Page_0_document_2_desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "HighEdWeb Link"
});